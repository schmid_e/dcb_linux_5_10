#!/bin/sh

# . /opt/poky-lsb/2.7/environment-setup-cortexa9t2hf-neon-poky-linux-gnueabi
export PATH=/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/usr/bin:/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/usr/sbin:/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/bin:/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/sbin:/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/usr/bin/../x86_64-pokysdk-linux/bin:/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-gnueabi:/opt/poky-lsb/2.7/sysroots/x86_64-pokysdk-linux/usr/bin/arm-poky-linux-musl:$PATH
#export PATH=/opt/Xilinx/SDK/2017.4/gnu/aarch32/lin/gcc-arm-linux-gnueabi/bin:$PATH
export CROSS_COMPILE=arm-poky-linux-gnueabi-
export ARCH=arm
reset
#make distclean
#make zynq_psi_wavedaq_dcb_defconfig
time make -j 8 UIMAGE_LOADADDR=0x8000 uImage
#make zynq_psi_wavedaq_dcb.dtb
# make zynq-zed-adv7511.dtb
#ls
#cp arch/arm/boot/uImage /data/export/develop/uImage_dev
