// WaveDAQ WDB and TCB flash devices accessed through backplane

#include <linux/mtd/spi-nor.h>

#include "core.h"

static const struct flash_info wavedaq_parts[] = {
	/* Flash of WDAQ cards in slot based on w25q128 and n25q128a13 but defined as nojedec to avoid scan */
	{ "wdb-flash-nonjedec", INFO(0, 0, 64 * 1024, 256, SECT_4K | SPI_NOR_DUAL_READ | SPI_NOR_QUAD_READ) },
	{ "tcb-flash-nonjedec", INFO(0, 0, 64 * 1024, 256, SPI_NOR_QUAD_READ | USE_FSR) },
	/* original setup { "tcb-flash-nonjedec", INFO(0, 0, 64 * 1024, 256, SECT_4K | SPI_NOR_QUAD_READ | USE_FSR | SPI_NOR_HAS_LOCK) }, */
};

const struct spi_nor_manufacturer spi_nor_wavedaq = {
	.name = "xmc",
	.parts = wavedaq_parts,
	.nparts = ARRAY_SIZE(wavedaq_parts),
};
